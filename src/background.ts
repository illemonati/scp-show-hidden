//@ts-ignore
browser.menus = browser.menus || browser.contextMenus;

const onCreated = () => {
    if (browser.runtime.lastError) {
        console.log('error creating item:' + browser.runtime.lastError);
    } else {
        console.log('item created successfully');
    }
};

browser.menus.create(
    {
        id: 'show-hidden-scp-content',
        title: 'SEE',
        documentUrlPatterns: ['*://*.scp-wiki.net/*'],
    },
    onCreated
);

browser.menus.onClicked.addListener((info, tab) => {
    switch (info.menuItemId) {
        case 'show-hidden-scp-content':
            browser.tabs.executeScript(tab.id, {
                file: 'changePage.js',
            });
            break;
    }
});
