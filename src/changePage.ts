console.log('YOU SHALL SEE');

const cssText = `
    display: block !important;
    font-size: revert !important;
    color: blue;
`;

const clearBackGroundCss = `
    background-color: revert !important;
`;

document
    .getElementById('page-content')
    ?.querySelectorAll('*')
    .forEach(async (e) => {
        const computedStyle = window.getComputedStyle(e);
        if (!(e instanceof HTMLElement)) return;
        if (e.getBoundingClientRect().width === 0) {
            e.style.cssText += cssText;
        }
        if (
            computedStyle.backgroundColor === computedStyle.color ||
            (computedStyle.backgroundColor.match(/rgba\(.+0\)/) &&
                computedStyle.color === 'rgb(255, 255, 255)')
        ) {
            e.style.cssText += cssText + clearBackGroundCss;
        }
    });

const infoDiv = document.querySelector('.info-container');
if (infoDiv && infoDiv instanceof HTMLElement) {
    infoDiv.style.display = 'none';
}
